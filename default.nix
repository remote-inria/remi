{
  pkgs ? import <nixpkgs> { },
}:
let
  py = pkgs.python3Packages;
in
py.buildPythonApplication {
  pname = "remote-inria";
  version = "1.7.4";

  src = ./.;

  pyproject = true;

  build-system = [ py.setuptools ];

  dependencies = with py; [
    pyyaml
    click
    termcolor
  ];
}
