# TODO

## TODO
- [ ] Add the option to build a container with `--sandbox`
- [ ] The default script should be `remi.sh` and should be generated at `remi init`
- [ ] For tmux, we should add the job to the session (as a new window) if it already exists, not
    crash
- [x] Support the gricad infrastructure:
    - [x] Update default config
    - [x] Embed the annoying gricad ssh commands within `remi` instead of requiring the user to have
      it set up in its `~/.ssh/config`
    - [x] init: ask if the user wants gricad support
    - [x] use `remg` commands instead ? -> No, bad idea
    - [x] Commands:
        - `setup`
        - cluster: `script`, `command` `interactive`
        - `connect`, `kill`
        - `push`, `pull`, `clean`
- [ ] Support more OS:
    - [ ] MacOS
    - [ ] Windows

## Done

- [x] In `remi push·` only write to `last_build.log` after the actual push has succeeded.
- [x] Option to directly attach to the tmux session after starting the job (`remi desktop -b -a`)
- [x] [DOC] update doc (especially for `remi desktop -b` and `remi desktop attach-session`)
- [x] command to kill a cluster job (`remi cluster kill [job_id]`)
- [x] Update documentation (README.md)
    - [x] Add documentation for venv (don't forget a tutorial on how to install conda)
    - [x] Find another way to display command description. The table is not very nice.
    - [x] Add option documentation for `remi desktop -b`.
    - [x] The `notebooks/` directory (`pull` it and do not put notebooks elsewhere...)
    - [x] update documentation for virtualenv support.
    - [x] Detail the 'push' strategy (not meant to be a backup, use of `--delete`, remote copy
      should not be modified, use of `exclude.txt`, ...)
- [x] Ability to kill a job that was launch in background on the remote (from `remi desktop -b`). I
  have honestly no idea on how to do that. (maybe using its PID ?) --> done through `tmux` or
  `screen`
- [x] Find a new name to replace _percu_ (Perception will become RobotLearn + this tool might be
  used by other teams at Inria) --> _remi_ (Remote Inria).
- [x] [BUG] Ctrl-c does not kill a non-background job.
- [x] Homogenize the logging formatting (what is in bold, what is between `` etc...)
- [x] Better support for virtual env:
    - [x] `conda` `environment.yaml` kind of file
    - [x] run `conda init bash` the first time --> We let the user install `conda` by himself and
      suppose that it usable.
- [x] Split `cli.py` in multiple files.
- [x] [Feature] Add tensorboard support.
- [x] [BUG] first push doesn't work at the very first time
- [x] Set singularity environment variables for tmp and cache directories.
- [x] Generic clean function ? `remi remote-clean PATH` == `remi rc PATH`
- [x] Isn't local clean useless ?
- [x] The default config file should be a copy of a template (instead of writing in a file from
  python). Hence we can benefit from comments.
- [x] [BUG] notebook created on remote will be deleted at the next push !\
Either create a dedicated `notebook/` folder or find a smarter idea...\
Maybe print a warning message to tell this to the user.\
At the end of a jupyter session, pull the `notebook_directory` (set in the config file) back to the
local machine.
- [x] Avoid all the duplicates in `cli.py`.
- [x] `remi` from inria desktop:\
If directly working from inria remote, no need to push.\
=> Detect localhost to know if working from inria. If so, no push, `desktop` commands do not ssh
anywhere (we may even disable them) and `cluster` commands do not push\
Print message that the situation has been detected.
- [x] `# Default hostname` code is redondant (something like 6-7 copies)
- [x] Swap the current 'dirty' (well it's pretty !) logging method to a more 'conventional' one
  (using the `logging` module). --> no, it is fine.
- [x] Implement venv support for `remi -i`.
- [x] Write the help message (the one displayed when running `remi` without arguments)
- [x] Recheck every name (config, commands and options) to make sure that they are as user-friendly
  as possible
- [x] Support Yihong's advanced use of oarsub.
- [x] Cluster: add an cli option to set the name of the job. Maybe use this name for the `stdout`
  and `stderr` location...
- [x] Implement the `update_packages()` function (for execution on remote)
- [x] Maybe `--no-build` should be true by default when using `-c` --> **implement a smart-build
  behaviour**
- [x] Manage virtual environments when executing code on remote (maybe also add cli options...).
- [x] Migrate from GitHub to gitlab.inria.fr
- [x] Set the default remote path as `/scratch/MACHINE/USERNAME/.remi_projects/PROJECT_NAME`
- [x] **BUG:** For some strange reason, when `remi build-container` re-build the container even if
  it already exists. Before it was not doing that...
- [x] Rename `main.py` to `core.py`
- [x] Do sweet and nice logging (especially to print the cluster information: id, gpu etc...)
- [x] Remove `job_type` config and replace it with 2 booleans `besteffort` and `idempotent`
- [x] `cd` in the project directory when using `remi cluster -i` (maybe not feasible)
- [x] Add a cli option for executing a command on the remote or on the cluster (`remi cluster -c
  'echo toto'`)
- [x] Possibility to use the singularity container on the remote (attention: in this case,
  systematically put `--nv` to the singularity command)
- [x] Interactive mode: for desktop, it boils down to `ssh`. For the cluster, it runs `oarsub -I`.
- [x] Option to provide a custom `-p` for `oarsub` (from the config file AND/OR in cli)
- [x] Custom bind on singularity (from the config file)
- [x] Freeze the apparition order of commands `remi --help` (instead of letting it display them in
  alphabetical order).
