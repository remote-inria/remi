#!/bin/sh

echo "Date: $(date)"
echo "Hostname: $(hostname)"
echo "user: $(whoami)"
echo "cwd: $(pwd)"
echo "nvidia info"
nvidia-smi
which python

# Simulation of a job that takes some time to complete and produces output.
for ((i = 0 ; i < 10 ; i++)); do
	echo $i
    sleep 1
done
